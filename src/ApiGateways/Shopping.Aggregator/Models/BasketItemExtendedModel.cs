﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopping.Aggregator.Models
{
    //retrieves additional data from the basket 
    public class BasketItemExtendedModel
    {
        //from shoppingCart class - mirrors it
        public int Quantity { get; set; }
        public string Color { get; set; }
        public decimal Price { get; set; }
        public string ProductId { get; set; }  //will be used to retrieve calalog products from mongodb
        public string ProductName { get; set; }

        //Product Related Additional Fields
        //these are the data that will be retrieved using thre product id above
        public string Category { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string ImageFile { get; set; }
    }
}
