﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBus.Messages.Common
{
    //this will be the queue name in rabbitmq
    //this is like appsettings
    
    public static class EventBusConstants
    {
        public const string BasketCheckoutQueue = "basketcheckout-queue";
    }
}
