﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Discount.API.Extensions
{
    public static class HostExtension
    {
        public static IHost MigrateDatabase<TContext>(this IHost host, int? retry = 0)
        {
            //retry count , i.e we'll retry the migration 
            //since the app is going to be starting under docker, there is no guarantee that postgreql will be
            //ready at the point of running this migration
            //hence the need for a retry mechanism
            int retryForAvailability = retry.Value;
            using (var scope = host.Services.CreateScope())
            {
                //get some services from asp.net dependency injection using the scope
                //eg service, configuration
                var services = scope.ServiceProvider;
                var configuration = services.GetRequiredService<IConfiguration>();
                var logger = services.GetRequiredService<ILogger<TContext>>();
                try
                {
                    logger.LogInformation("Migrating postgresql database");
                    using var connection = new NpgsqlConnection(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
                    connection.Open();  //cos we'll be performing db operations

                    //create new command
                    using var command = new NpgsqlCommand
                    {
                        Connection = connection
                    };

                    command.CommandText = "DROP TABLE IF EXISTS Coupon";
                    command.ExecuteNonQuery();

                    //create table
                    command.CommandText = @"CREATE TABLE Coupon(Id SERIAL PRIMARY KEY,
                                                                    ProductName VARCHAR(24) NOT NULL, 
                                                                     Description TEXT, Amount INT)";
                    command.ExecuteNonQuery();

                    //SEED DATABASE
                    command.CommandText = "INSERT INTO Coupon(ProductName, Description, Amount) VALUES('Iphone 6s', 'IPhone discount', 200);";
                    command.ExecuteNonQuery();
                    command.CommandText = "INSERT INTO Coupon(ProductName, Description, Amount) VALUES('Samsung Galaxy', 'Christmas promo', 250);";
                    command.ExecuteNonQuery();

                    logger.LogInformation("Migration completed");
                }
                catch (NpgsqlException ex)
                {

                    logger.LogInformation(ex, "An error occured migrating postgresql database");

                    //retry the migration
                    if (retryForAvailability < 50)
                    {
                        retryForAvailability++;
                        System.Threading.Thread.Sleep(200);
                        MigrateDatabase<TContext>(host, retryForAvailability);
                    }
                }
            }

            //return host information
            return host;
        }
    }
}
