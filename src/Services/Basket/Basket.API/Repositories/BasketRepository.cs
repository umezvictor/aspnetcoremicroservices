﻿using Basket.API.Entities;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basket.API.Repositories
{
    public class BasketRepository : IBasketRepository
    {
        //perform crud operations on the redis cache
        private readonly IDistributedCache _redisCache;

        public BasketRepository(IDistributedCache redisCache)
        {
            _redisCache = redisCache ?? throw new ArgumentNullException(nameof(redisCache));
        }

        public async Task DeleteBasket(string username)
        {
            await _redisCache.RemoveAsync(username);
        }

        public async Task<ShoppingCart> GetBasket(string username)
        {
            //basket is a set of key value pair from redis database (cache)
            var basket = await _redisCache.GetStringAsync(username);
            if (String.IsNullOrEmpty(basket))
                return null;
            return JsonConvert.DeserializeObject<ShoppingCart>(basket);
        }


        //handles add, create and update basket operations
        public async Task<ShoppingCart> UpdateBasket(ShoppingCart basket)
        {
            //create a key value pair
            //key = username, value is the entire ShoppingCark basket object
            //string basketJson = JsonConvert.SerializeObject(basket);
             await _redisCache.SetStringAsync(basket.UserName, JsonConvert.SerializeObject(basket));          
            // var objectValue = Encoding.UTF8.GetBytes(basketJson);
            // await _redisCache.SetAsync(basket.UserName, objectValue);

           /* var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1),
                SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            // Step 3
           *//* await _redisCache.SetStringAsync(basket.UserName, basketJson, new DistributedCacheEntryOptions()
            {
                AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(5)
            });*//*

            await _redisCache.SetStringAsync(basket.UserName, basketJson, options);*/

            return await GetBasket(basket.UserName);
        }
    }
}
