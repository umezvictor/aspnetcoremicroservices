﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Application.Features.Orders.Queries.GetOrdersList
{
    //request class for our query object
    //think of this as an api dto object ==> GetOrdersListQuery
    //List<OrdersVm> means this class will be expecting a response of List<OrdersVm> from the query handler
    public class GetOrdersListQuery : IRequest<List<OrdersVm>>
    {
        //mediatr will trigger this handle classes when a request comes
        public string UserName { get; set; }
        public GetOrdersListQuery(string userName)
        {
            UserName = userName ?? throw new ArgumentNullException(nameof(userName));
        }
    }
}
