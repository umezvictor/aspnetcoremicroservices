﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Ordering.Application.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Application
{
    //this is where I registrered all services used for handler classes, validators etc
    //this is a central class for adding every service I want in startup.cs
    public static class ApplicationServiceRegistration
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            //register automapper
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            //register fluent validator
            //checks if there is any class inheriting from the iabstractvalidator
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            //mediatr
            services.AddMediatR(Assembly.GetExecutingAssembly());

            //pipeline nehaviours
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            return services;

        }
    }
}
