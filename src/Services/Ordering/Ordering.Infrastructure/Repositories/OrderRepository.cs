﻿using Microsoft.EntityFrameworkCore;
using Ordering.Application.Contracts.Persistence;
using Ordering.Domain.Entities;
using Ordering.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Infrastructure.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        //I added this constructor, to obey oop rules
        //when you have a base class with a constructor and parameter
        //you have to add that parameter to the constructor of your sub class
        public OrderRepository(OrderContext dbContext) : base(dbContext)
        {
        }
        //https://sendgrid.com/docs/for-developers/sending-email/v3-csharp-code-example
        //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
        public async Task<IEnumerable<Order>> GetOrdersByUserName(string userName)
        {
            var orderList = await _dbContext.Orders
                                .Where(o => o.UserName == userName)
                                .ToListAsync();
            return orderList;
        }
    }
}
